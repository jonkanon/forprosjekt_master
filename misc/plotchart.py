import pandas as pd
import matplotlib.pyplot as plt

# Read the CSV file into a DataFrame
df = pd.read_csv('file.csv')

# Set the "Language pair" column as the index for better plotting
df.set_index("Word pair", inplace=True)

# Plotting
plt.figure(figsize=(12, 8))

colors = ['#3498db', '#e74c3c']

# Plot bars for each language pair
df.plot(kind='bar', width=0.8, color=colors)

# Set labels and title
plt.xlabel('Word pair')
plt.ylabel("Manhattan distance")

# Add legend
plt.legend(["Without context", "With context"])

# Rotate x-axis labels for better readability
plt.xticks(rotation=45, ha='right')

# Show the plot
plt.show()

