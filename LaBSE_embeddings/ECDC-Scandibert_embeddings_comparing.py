import os
from translate.storage.tmx import tmxfile
import torch
from transformers import AutoTokenizer, AutoModel
import numpy as np


def doubleprint(*toprint, file):
    toprint = (str(e) for e in toprint)
    toprint = " ".join(toprint)
    print(toprint)
    print(toprint, file=file)


pairs = {
    "en-nb": {"en": [], "nb": []},
    "en-da": {"en": [], "da": []},
    "en-sv": {"en": [], "sv": []},
    "nb-da": {"nb": [], "da": []},
    "nb-sv": {"nb": [], "sv": []},
    "sv-da": {"sv": [], "da": []}
}

folder = "ecdc-tm"

for file in os.listdir(folder):
    sourcelang, targetlang = file.split(".")[0].split("-")
    print("importing", sourcelang, targetlang)
    with open(folder+"/"+file, "r") as f:

        tmx_file = tmxfile(f, sourcelang, targetlang)
    for node in tmx_file.unit_iter():
        pairs[file.split(".")[0]][sourcelang].append(node.source)
        pairs[file.split(".")[0]][targetlang].append(node.target)

tall = 25
print(pairs["sv-da"]["sv"][tall])
print(pairs["sv-da"]["da"][tall])
for key in pairs.keys():
    print(key, len(pairs[key][key.split("-")[0]]))

model = torch.load("../ScandiBERT/pytorch_model.bin")

model_name = "../ScandiBERT"
tokenizer = AutoTokenizer.from_pretrained(model_name)
model = AutoModel.from_pretrained(model_name)

numberofsentencepairs = 100
iterations = 20

offset = 0

with open("results/ecdc_scandi_" + str(numberofsentencepairs) + "x" + str(iterations) +
          ".txt", "w") as f:

    for pair in pairs:
        doubleprint(pair, file=f)
        sourcelang, targetlang = pairs[pair].keys()
        euclideandistances = []
        manhattandistances = []
        fractionaldistances = []

        crashes = 0

        for it in range(0, iterations):
            start = numberofsentencepairs*it
            end = numberofsentencepairs*(it+1) - 1
            start += offset
            end += offset
            print(start, end)
            sourcesentences = pairs[pair][sourcelang][start:end]
            targetsentences = pairs[pair][targetlang][start:end]
            # print(sourcesentences[0])
            # print(targetsentences[0])

            source_embeds = []
            target_embeds = []

            for i in range(len(sourcesentences)):
                input1 = tokenizer(sourcesentences[i], return_tensors="pt",
                                   padding=True, truncation=False)
                input2 = tokenizer(targetsentences[i], return_tensors="pt",
                                   padding=True, truncation=False)
                with torch.no_grad():
                    try:
                        output1 = model(**input1)
                        output2 = model(**input2)
                        sentence_embedding1 = output1.last_hidden_state.mean(dim=1)
                        sentence_embedding2 = output2.last_hidden_state.mean(dim=1)

                        manhattandistances.append(torch.norm(sentence_embedding1 -
                                                             sentence_embedding2,
                                                             p=1).item())
                        euclideandistances.append(torch.norm(sentence_embedding1 -
                                                             sentence_embedding2,
                                                             p=2).item())
                        fractionaldistances.append(torch.norm(sentence_embedding1 -
                                                              sentence_embedding2,
                                                              p=0.5).item())
                    except Exception:
                        crashes += 1
                        continue

            # avgsman.append(sum(manhattandistances)/len(manhattandistances))
            # avgseuc.append(sum(euclideandistances)/len(euclideandistances))
            # avgsfrac.append(sum(fractionaldistances)/len(fractionaldistances))
            #
            # rankeddistances = []
            # for i in range(0, len(euclideandistances)):
            #     rankeddistances.append({"index": i,
            #                             "distance": euclideandistances[i],
            #                             "sentence_source": sourcesentences[i],
            #                             "sentence_target": targetsentences[i]
            #                             })
            # sorted_distances = sorted(rankeddistances, key=lambda x: x['distance'])

            """for e in sorted_distances:
                print("Distance:", e["distance"])
                print(e["sentence_source"])
                print(e["sentence_target"])
                print()"""

        doubleprint("Average manhattan distance for " + pair + ":", file=f)
        doubleprint(np.average(manhattandistances), file=f)
        doubleprint("Standard deviation:", np.std(manhattandistances), file=f)

        doubleprint("Average euclidean distance for " + pair + ":", file=f)
        doubleprint(np.average(euclideandistances), file=f)
        doubleprint("Standard deviation:", np.std(euclideandistances), file=f)

        doubleprint("Average 0.5 distance for " + pair + ":", file=f)
        doubleprint(np.average(fractionaldistances), file=f)
        doubleprint("Standard deviation:", np.std(fractionaldistances), file=f)
        doubleprint(file=f)
