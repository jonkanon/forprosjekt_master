#!/bin/bash
# befure running, make sure that all 3 files in the russian-english parallel corpus are in the same directory as this script
# the corpus can be downloaded from https://conferences.unite.un.org/UNCORPUS/Home/DownloadOverview

cat UNv1.0.en-ru.tar.gz.00 UNv1.0.en-ru.tar.gz.01 UNv1.0.en-ru.tar.gz.02 > UNv1.0.en-ru.tar.gz
tar -xvf UNv1.0.en-ru.tar.gz
cd en-ru

#shorten file to only have 10.000 lines
cat UNv1.0.en-ru.en | head -n 10000 > UNv1.0.en-ru.en.short
cat UNv1.0.en-ru.ru | head -n 10000 > UNv1.0.en-ru.ru.short
