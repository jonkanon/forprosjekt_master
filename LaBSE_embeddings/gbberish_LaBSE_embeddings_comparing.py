import tensorflow_hub as hub
import tensorflow as tf # noqa
import tensorflow_text as text  # Needed for loading universal-sentence-encoder-cmlm/multilingual-preprocess # noqa
import numpy as np
import os
from translate.storage.tmx import tmxfile
import random
import copy


def doubleprint(*toprint, file):
    toprint = (str(e) for e in toprint)
    toprint = " ".join(toprint)
    print(toprint)
    print(toprint, file=file)


pairs = {
    "en-nb": {"en": [], "nb": []},
    "en-nn": {"en": [], "nn": []},
    "nb-nn": {"nb": [], "nn": []}
}

for root, dirs, filenames in os.walk("maalfrid"):
    if "-" in root:  # in a subfolder with tmx pair files
        folder = root.split("/")[1]
        sourcelang, targetlang = folder.split("-")
        for filename in filenames:
            with open(root + "/" + filename, "rb") as f:
                tmx_file = tmxfile(f, sourcelang, targetlang)

            for node in tmx_file.unit_iter():
                pairs[folder][sourcelang].append(node.source)
                pairs[folder][targetlang].append(node.target)

scrambled = copy.deepcopy(pairs["en-nb"]["en"])
random.shuffle(scrambled)

pairs = {
    "en-scrambled": {"en": pairs["en-nb"]["en"], "scrambled": scrambled}
}                # import preprocessor and LaBSE model
preprocessor = hub.KerasLayer(
    "https://tfhub.dev/google/universal-sentence-encoder-cmlm/multilingual-preprocess/2") # noqa
encoder = hub.KerasLayer("https://tfhub.dev/google/LaBSE/2")

numberofsentencepairs = 100
iterations = 100

fraction = 0.5

with open("results/gibberish_" + str(numberofsentencepairs) + "x" + str(iterations) +
          ".txt", "w") as f:
    from scipy.spatial.distance import cityblock
    for pair in pairs:
        doubleprint(pair, file=f)
        sourcelang, targetlang = pairs[pair].keys()
        avgsman = []
        avgseuc = []
        avgsfrac = []

        for it in range(0, iterations):
            start = numberofsentencepairs*it
            end = numberofsentencepairs*(it+1) - 1
            print(start, end)
            sourcesentences = pairs[pair][sourcelang][start:end]
            targetsentences = pairs[pair][targetlang][start:end]
            print(sourcesentences[0])
            print(targetsentences[0])
            source_embeds = encoder(preprocessor(sourcesentences))["default"]
            target_embeds = encoder(preprocessor(targetsentences))["default"]

            euclideandistances = []
            manhattandistances = []
            fracdistances = []
            for i in range(0, len(source_embeds)):
                manhattandistances.append(cityblock(source_embeds[i],
                                                    target_embeds[i]))
                euclideandistances.append(np.linalg.norm(source_embeds[i] -
                                                         target_embeds[i]))
                fracdistances.append(np.linalg.norm(source_embeds[i] -
                                                    target_embeds[i],
                                                    ord=fraction))

            avgsman.append(sum(manhattandistances)/len(manhattandistances))
            avgseuc.append(sum(euclideandistances)/len(euclideandistances))
            avgsfrac.append(sum(fracdistances)/len(fracdistances))

        doubleprint("Average manhattan distance for " + pair + ":", file=f)
        doubleprint(sum(avgsman)/len(avgsman), file=f)
        doubleprint("Average euclidean distance for " + pair + ":", file=f)
        doubleprint(sum(avgseuc)/len(avgseuc), file=f)
        doubleprint("Average L_" + str(fraction) + " distance for "
                    + pair + ":", file=f)
        doubleprint(sum(avgsfrac)/len(avgsfrac), file=f)

        doubleprint(file=f)
