
def doubleprint(*toprint, file):
    toprint = (str(e) for e in toprint)
    toprint = " ".join(toprint)
    print(toprint)
    print(toprint, file=file)


pairs = {
    "en-ru": {"en": [], "ru": []}
}

langs = ["en", "ru"]

for e in langs:
    with open("en-ru/UNv1.0.en-ru." + e + ".short", encoding="utf-8", mode="r") as f:
        pairs["en-ru"][e] = f.readlines()


#copied from https://tfhub.dev/google/LaBSE/2

import tensorflow_hub as hub
import tensorflow as tf
import tensorflow_text as text  # Needed for loading universal-sentence-encoder-cmlm/multilingual-preprocess
import numpy as np

# import preprocessor and LaBSE model
preprocessor = hub.KerasLayer(
    "https://tfhub.dev/google/universal-sentence-encoder-cmlm/multilingual-preprocess/2")
encoder = hub.KerasLayer("https://tfhub.dev/google/LaBSE/2")

numberofsentencepairs = 100
iterations = 100

fraction = 0.5

with open("results/en-ru_" + str(numberofsentencepairs) + "x" + str(iterations) + ".txt", "w") as f:
    from scipy.spatial.distance import cityblock

    for pair in pairs:
        doubleprint(pair, file=f)
        sourcelang, targetlang = pairs[pair].keys()
        euclideandistances = []
        manhattandistances = []
        fracdistances = []

        for it in range(0, iterations):
            start = numberofsentencepairs*it
            end = numberofsentencepairs*(it+1) - 1
            print(start, end)
            sourcesentences = pairs[pair][sourcelang][start:end]
            targetsentences = pairs[pair][targetlang][start:end]
            print(sourcesentences[0])
            print(targetsentences[0])
            source_embeds = encoder(preprocessor(sourcesentences))["default"]
            target_embeds = encoder(preprocessor(targetsentences))["default"]

            for i in range(0, len(source_embeds)):
                manhattandistances.append(cityblock(source_embeds[i],
                                                    target_embeds[i]))
                euclideandistances.append(np.linalg.norm(source_embeds[i] -
                                                         target_embeds[i]))
                fracdistances.append(np.linalg.norm(source_embeds[i] -
                                                    target_embeds[i],
                                                    ord=fraction))

        doubleprint("Average manhattan distance for " + pair + ":", file=f)
        doubleprint(np.average(manhattandistances), file=f)
        doubleprint("Standard deviation:", np.std(manhattandistances), file=f)

        doubleprint("Average euclidean distance for " + pair + ":", file=f)
        doubleprint(np.average(euclideandistances), file=f)
        doubleprint("Standard deviation:", np.std(euclideandistances), file=f)

        doubleprint("Average 0.5 distance for " + pair + ":", file=f)
        doubleprint(np.average(fracdistances), file=f)
        doubleprint("Standard deviation:", np.std(fracdistances), file=f)

        doubleprint(file=f)
