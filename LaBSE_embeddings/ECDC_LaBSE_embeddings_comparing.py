import os
from translate.storage.tmx import tmxfile

import tensorflow_hub as hub
import tensorflow as tf
import tensorflow_text as text  # Needed for loading universal-sentence-encoder-cmlm/multilingual-preprocess # noqa
import numpy as np
from similarity_metrics import Similarity


def doubleprint(*toprint, file):
    toprint = (str(e) for e in toprint)
    toprint = " ".join(toprint)
    print(toprint)
    print(toprint, file=file)


# builds datastructure of translation pairs

pairs = {
    "en-nb": {"en": [], "nb": []},
    "en-da": {"en": [], "da": []},
    "en-sv": {"en": [], "sv": []},
    "nb-da": {"nb": [], "da": []},
    "nb-sv": {"nb": [], "sv": []},
    "sv-da": {"sv": [], "da": []}
}
folder = "ecdc-tm"

for file in os.listdir(folder):
    sourcelang, targetlang = file.split(".")[0].split("-")
    print("importing", sourcelang, targetlang)
    with open(folder+"/"+file, "r") as f:

        tmx_file = tmxfile(f, sourcelang, targetlang)
    for node in tmx_file.unit_iter():
        pairs[file.split(".")[0]][sourcelang].append(node.source)
        pairs[file.split(".")[0]][targetlang].append(node.target)

# copied from https://tfhub.dev/google/LaBSE/2
# import preprocessor and LaBSE model
preprocessor = hub.KerasLayer(
    "https://tfhub.dev/google/universal-sentence-encoder-cmlm/multilingual-preprocess/2") # noqa
encoder = hub.KerasLayer("https://tfhub.dev/google/LaBSE/2")

numberofsentencepairs = 100
iterations = 20

fraction = 0.5

with open("results/ecdc-" + str(numberofsentencepairs) + "x" + str(iterations)
          + ".txt", "w") as f:

    from scipy.spatial.distance import cityblock

    for pair in pairs:
        doubleprint(pair, file=f)
        sourcelang, targetlang = pairs[pair].keys()
        avgsman = []
        avgseuc = []
        avgsfrac = []

        for it in range(0, iterations):
            start = numberofsentencepairs*it
            end = numberofsentencepairs*(it+1) - 1
            print(start, end)
            sourcesentences = pairs[pair][sourcelang][start:end]
            targetsentences = pairs[pair][targetlang][start:end]
            print(sourcesentences[0])
            print(targetsentences[0])
            source_embeds = encoder(preprocessor(sourcesentences))["default"]
            target_embeds = encoder(preprocessor(targetsentences))["default"]

            euclideandistances = []
            manhattandistances = []
            fracdistances = []
            for i in range(0, len(source_embeds)):
                manhattandistances.append(cityblock(source_embeds[i],
                                                    target_embeds[i]))
                euclideandistances.append(np.linalg.norm(source_embeds[i] -
                                                         target_embeds[i]))
                fracdistances.append(np.linalg.norm(source_embeds[i] -
                                                    target_embeds[i],
                                                    ord=fraction))

            avgsman.append(sum(manhattandistances)/len(manhattandistances))
            avgseuc.append(sum(euclideandistances)/len(euclideandistances))
            avgsfrac.append(sum(fracdistances)/len(fracdistances))

        doubleprint("Average manhattan distance for " + pair + ":", file=f)
        doubleprint(np.average(manhattandistances), file=f)
        doubleprint("Standard deviation:", np.std(manhattandistances), file=f)

        doubleprint("Average euclidean distance for " + pair + ":", file=f)
        doubleprint(np.average(euclideandistances), file=f)
        doubleprint("Standard deviation:", np.std(euclideandistances), file=f)

        doubleprint("Average 0.5 distance for " + pair + ":", file=f)
        doubleprint(np.average(fracdistances), file=f)
        doubleprint("Standard deviation:", np.std(fracdistances), file=f)

        doubleprint(file=f)
